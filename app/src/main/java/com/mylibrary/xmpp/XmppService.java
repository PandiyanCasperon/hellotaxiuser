package com.mylibrary.xmpp;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;

import com.hellotaxiuser.utils.ConnectionDetector;
import com.hellotaxiuser.utils.SessionManager;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by Prem Kumar and Anitha on 12/15/2016.
 */

public class XmppService extends Service {

    public static final long INTERVAL = 1000;//variable to execute services every 1 second
    public static MyXMPP xmpp;
    private String ServiceName = "", HostAddress = "";
    private String USERNAME = "";
    private String PASSWORD = "";
    private SessionManager sessionManager;
    private Handler mHandler = new Handler(); // run on another Thread to avoid crash
    private Timer mTimer = null;

    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;

    @Override
    public IBinder onBind(final Intent intent) {
        return new LocalBinder<XmppService>(this);
    }

    @Override
    public boolean onUnbind(final Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onCreate() {

        try {


            sessionManager = new SessionManager(XmppService.this);

            // get user data from session
            HashMap<String, String> domain = sessionManager.getXmpp();
            ServiceName = domain.get(SessionManager.KEY_HOST_NAME);
            HostAddress = domain.get(SessionManager.KEY_HOST_URL);

            HashMap<String, String> user = sessionManager.getUserDetails();
            USERNAME = user.get(SessionManager.KEY_USERID);
            PASSWORD = user.get(SessionManager.KEY_XMPP_SEC_KEY);

            System.out.println("----------xmpp ServiceName------------" + ServiceName);
            System.out.println("----------xmpp HostAddress------------" + HostAddress);
            System.out.println("----------xmpp USERNAME------------" + USERNAME);
            System.out.println("----------xmpp PASSWORD------------" + PASSWORD);

            xmpp = MyXMPP.getInstance(XmppService.this, ServiceName, HostAddress, USERNAME, PASSWORD);
            xmpp.connect("onCreate");

            mTimer = new Timer(); // recreate new timer
            mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, INTERVAL);

            System.out.println("--------------Xmpp Service Created-----------");

        }catch (NullPointerException e){
            e.printStackTrace();
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int onStartCommand(final Intent intent, final int flags,
                              final int startId) {
        System.out.println("--------------Xmpp Service Started-----------");
        return Service.START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTimer != null)
            mTimer.cancel();
        /*xmpp.connection.disconnect();
        xmpp.instance = null;
        xmpp.connected =false;
        xmpp.loggedin =false;
        xmpp.isconnecting =false;
        xmpp.chat_created =false;
        xmpp.isToasted =true;
        xmpp.instanceCreated =true;*/
        System.out.println("--------------Xmpp Service Stopped-----------");
    }


    private class TimeDisplayTimerTask extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @TargetApi(Build.VERSION_CODES.O)
                @Override
                public void run() {
                    if (sessionManager.isLoggedIn()) {
                        try {
                            cd = new ConnectionDetector(getBaseContext());
                            isInternetPresent = cd.isConnectingToInternet();
                            if (isInternetPresent) {
                                try {
                                    xmpp = MyXMPP.getInstance(getBaseContext(), ServiceName, HostAddress, USERNAME, PASSWORD);
                                    xmpp.connect("onCreate");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }


}
