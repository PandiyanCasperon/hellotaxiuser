package com.mylibrary.pushnotification;


import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;

import com.hellotaxiuser.app.FareBreakUp;
import com.hellotaxiuser.app.MyRideDetailTrackRide;
import com.hellotaxiuser.app.MyRideRating;
import com.hellotaxiuser.app.R;
import com.hellotaxiuser.iconstant.Iconstant;
import com.hellotaxiuser.pojo.ChatPojo;
import com.hellotaxiuser.utils.SessionManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mylibrary.sqliteDb.ChatDatabaseHelper;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by user145 on 8/4/2017.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final int NOTIFICATION_ID = 1;
    NotificationCompat.Builder builder;
    private String key1 = "", key2 = "", key3 = "", key4 = "", key5 = "",
            key6 = "", key7 = "", key8 = "", key9 = "", key10 = "",
            key11 = "", key12 = "", message = "", key13 = "", key14 = "", key15 = "", ride_id = "", message_desc = "", timestamppush = "";
    String action = "", msg1, title, banner;
    private static SessionManager Session;
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private String Job_status_message = "";
    private static final String FCM_TOKEN = "FCM_TOKEN_ID";


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());


        Session = new SessionManager(getApplicationContext());
        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {


                Map<String, String> params = remoteMessage.getData();
                JSONObject object = new JSONObject(params);
                Log.d("JSON_OBJECT", object.toString());

                handleDataMessage(object);
            } catch (Exception e) {
                Log.d(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotification(String message) {
        Job_status_message = message;

    }

    private void handleDataMessage(JSONObject json) {

        try {
            action = json.getString(Iconstant.Push_Action);

            if (action.equalsIgnoreCase(Iconstant.pushNotificationDriverLoc)) {

            } else {

                if (json.has("key1")) {
                    key1 = json.getString("key1");
                }
                if (json.has("key2")) {
                    key2 = json.getString("key2");
                }
                if (json.has("key3")) {
                    key3 = json.getString("key3");
                }
                if (json.has("key4")) {
                    key4 = json.getString("key4");
                }
                if (json.has("key5")) {
                    key5 = json.getString("key5");
                }
                if (json.has("key6")) {
                    key6 = json.getString("key6");
                }
                if (json.has("key7")) {
                    key7 = json.getString("key7");
                }
                if (json.has("key8")) {
                    key8 = json.getString("key8");
                }
                if (json.has("key9")) {
                    key9 = json.getString("key9");
                }


                if (json.has("message_desc")) {
                    message_desc = json.getString("message_desc");
                }
                if (json.has("timestamp")) {
                    timestamppush = json.getString("timestamp");
                }
                if (json.has("ride_id")) {
                    ride_id = json.getString("ride_id");
                }


                if (json.has("key10")) {
                    key10 = json.getString("key10");
                }
                if (json.has("key11")) {
                    key11 = json.getString("key11");
                }
                if (json.has("key12")) {
                    key12 = json.getString("key12");
                }
                if (json.has("key13")) {
                    key13 = json.getString("key13");
                }
                if (json.has("key14")) {
                    key14 = json.getString("key14");
                }
                if (json.has("key15")) {
                    key15 = json.getString("key15");
                }
                if (json.has(Iconstant.Push_Message)) {
                    message = json.getString(Iconstant.Push_Message);
                }

                sendNotification(message.toString());


            }
            if (action.equalsIgnoreCase(Iconstant.PushNotification_AcceptRide_Key)) {
                ChatDatabaseHelper db = new ChatDatabaseHelper(getApplicationContext());
                db.clearTable();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void sendNotification(String msg) {
        Intent notificationIntent = null;
        int id = createID();

        notificationIntent = new Intent(MyFirebaseMessagingService.this, MyRideDetailTrackRide.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notificationIntent.putExtra("type", "push");


        if (Iconstant.ACTION_CHAT_RECEIVED.equalsIgnoreCase(action)) {
            if (!timestamppush.equals("")) {
                if (!ride_id.equals("")) {
                    String timestamp = "";
                    ChatDatabaseHelper db;
                    SessionManager session;
                    session = new SessionManager(getApplicationContext());
                    db = new ChatDatabaseHelper(getApplicationContext());

                    SimpleDateFormat df_time = new SimpleDateFormat("hh:mm a");
                    Calendar cal = Calendar.getInstance();
                    String sCurrentTime = df_time.format(cal.getTime());
                    timestamp = sCurrentTime;

                    HashMap<String, String> state = session.getAppStatus();
                    String Str_driverState = state.get(SessionManager.KEY_APP_STATUS);
                    String sMessage1 = "";
                    sMessage1 = message_desc;

                    if (!timestamppush.equals("")) {
                        timestamp = timestamppush;
                    }

                    ChatDatabaseHelper mHelper = new ChatDatabaseHelper(getApplicationContext());
                    SQLiteDatabase dataBase = mHelper.getWritableDatabase();
                    int okl = 0;
                    Cursor mCursor = dataBase.rawQuery("SELECT * FROM " + mHelper.TABLE_CHAT + " WHERE chattimestamp ='" + timestamp + "'" + " AND chatMessage='" + sMessage1 + "'", null);
                    if (mCursor.moveToFirst()) {
                        do {
                            okl++;
                        } while (mCursor.moveToNext());
                    }
                    if (okl == 0) {
                        try {

                            db.addChat(new ChatPojo(sMessage1, "OTHER", sCurrentTime, "0", timestamp), getApplicationContext());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        if (action.equalsIgnoreCase(Iconstant.PushNotification_AcceptRide_Key)) {

            Intent local = new Intent();
            local.setAction("com.app.pushnotification.RideAccept");
            local.putExtra("driverID", key1);
            local.putExtra("driverName", key2);
            local.putExtra("driverEmail", key3);
            local.putExtra("driverImage", key4);
            local.putExtra("driverRating", key5);
            local.putExtra("driverLat", key6);
            local.putExtra("driverLong", key7);
            local.putExtra("driverTime", key8);
            local.putExtra("rideID", key9);
            local.putExtra("driverMobile", key10);
            local.putExtra("driverCar_no", key11);
            local.putExtra("driverCar_model", key12);
            local.putExtra("userLatitude", key14);
            local.putExtra("userLongitude", key15);
            local.putExtra("message", message);
            local.putExtra("Action", action);
            sendBroadcast(local);
        } else if (action.equalsIgnoreCase(Iconstant.PushNotification_CabArrived_Key)) {
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_Arrived_Driver");
            broadcastIntent.putExtra("driverLat", key3);
            broadcastIntent.putExtra("driverLong", key4);
            sendBroadcast(broadcastIntent);
            notificationIntent.putExtra("page", "track");
            notificationIntent.putExtra("rideID", key1);
        } else if (action.equalsIgnoreCase(Iconstant.pushNotificationBeginTrip)) {

            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip");
            broadcastIntent.putExtra("drop_lat", key3);
            broadcastIntent.putExtra("drop_lng", key4);
            broadcastIntent.putExtra("pickUp_lat", key5);
            broadcastIntent.putExtra("pickUp_lng", key6);
            broadcastIntent.putExtra("drop_locc", key7);
            sendBroadcast(broadcastIntent);
            notificationIntent.putExtra("page", "track");
            notificationIntent.putExtra("rideID", key1);

        } else if (action.equalsIgnoreCase(Iconstant.pushNotification_ReloadTrackingPage_Key)) {
            notificationIntent.putExtra("page", "track");
            notificationIntent.putExtra("rideID", key1);
        } else if (action.equalsIgnoreCase(Iconstant.PushNotification_RequestPayment_Key)) {
            notificationIntent = new Intent(MyFirebaseMessagingService.this, FareBreakUp.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            notificationIntent.putExtra("type", "push");
            notificationIntent.putExtra("page", "farebreakup");
            notificationIntent.putExtra("RideID", key6);
        } else if (action.equalsIgnoreCase(Iconstant.PushNotification_PaymentPaid_Key)) {
            notificationIntent = new Intent(MyFirebaseMessagingService.this, MyRideRating.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            notificationIntent.putExtra("type", "push");
            notificationIntent.putExtra("RideID", key1);
        } else if (action.equalsIgnoreCase(Iconstant.pushNotification_Ads)) {
            notificationIntent.putExtra("page", "Ads");
            notificationIntent.putExtra("title", key1);
            notificationIntent.putExtra("msg", key2);
            notificationIntent.putExtra("banner", key3);
        } else if (action.equalsIgnoreCase(Iconstant.PushNotification_RideCompleted_Key)) {
            notificationIntent.putExtra("page", "details");
            notificationIntent.putExtra("rideId", key1);
        } else if (action.equalsIgnoreCase(Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key)) {


            Intent finish_fareBreakUp = new Intent();
            finish_fareBreakUp.setAction("com.pushnotification.finish.FareBreakUp");
            sendBroadcast(finish_fareBreakUp);

            Intent finish_timerPage = new Intent();
            finish_timerPage.setAction("com.pushnotification.finish.TimerPage");
            sendBroadcast(finish_timerPage);

            Intent finish_pushAlert = new Intent();
            finish_pushAlert.setAction("com.pushnotification.finish.PushNotificationAlert");
            sendBroadcast(finish_pushAlert);

            Intent finish_MyRideDetails = new Intent();
            finish_MyRideDetails.setAction("com.pushnotification.finish.MyRideDetails");
            sendBroadcast(finish_MyRideDetails);

            Intent local = new Intent();
            local.setAction("COM.MYTRACKPAGE.FINISH");
            sendBroadcast(local);

            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction("com.pushnotification.updateBottom_view");
            sendBroadcast(broadcastIntent);
        } else if (action.equalsIgnoreCase(Iconstant.PushNotification_RideCancelled_Key)) {
            Intent finish_fareBreakUp = new Intent();
            finish_fareBreakUp.setAction("com.pushnotification.finish.FareBreakUp");
            sendBroadcast(finish_fareBreakUp);

            Intent finish_timerPage = new Intent();
            finish_timerPage.setAction("com.pushnotification.finish.TimerPage");
            sendBroadcast(finish_timerPage);

            Intent finish_pushAlert = new Intent();
            finish_pushAlert.setAction("com.pushnotification.finish.PushNotificationAlert");
            sendBroadcast(finish_pushAlert);

            Intent finish_MyRideDetails = new Intent();
            finish_MyRideDetails.setAction("com.pushnotification.finish.MyRideDetails");
            sendBroadcast(finish_MyRideDetails);

            Intent local = new Intent();
            local.setAction("COM.MYTRACKPAGE.FINISH");
            sendBroadcast(local);

            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction("com.pushnotification.updateBottom_view");
            sendBroadcast(broadcastIntent);
        }


        if (action.equals(Iconstant.PushNotification_PaymentPaid_Key) || action.equals(Iconstant.PushNotification_CabArrived_Key) || action.equals(Iconstant.pushNotificationBeginTrip) || action.equalsIgnoreCase(Iconstant.PushNotification_RequestPayment_Key)) {
            if (Build.VERSION.SDK_INT >= 26) {
                final NotificationManager mNotific =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                final int ncode = 101;
                final String ChannelID = "my_channel_01";
                CharSequence name = "pandiyan";
                String desc = "this is notific";
                int imp = NotificationManager.IMPORTANCE_HIGH;

                @SuppressLint("WrongConstant") NotificationChannel mChannel = new NotificationChannel(ChannelID, name,
                        imp);
                mChannel.setDescription(desc);
                mChannel.setLightColor(Color.CYAN);
                mChannel.canShowBadge();
                mChannel.setShowBadge(true);
                mNotific.createNotificationChannel(mChannel);

                Resources res = getApplicationContext().getResources();
                PendingIntent pendingIntent = PendingIntent.getActivity(this, id, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
                @SuppressLint("WrongConstant") Notification n = new Notification.Builder(this, ChannelID)
                        .setContentTitle(msg)
                        .setSmallIcon(R.drawable.newapplogo)
                        .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_logo))
                        .setContentText(Job_status_message)
                        .setBadgeIconType(R.drawable.app_logo)
                        .setNumber(5)
                        .setColor(getResources().getColor(R.color.app_color))
                        .setLights(0xffff0000, 100, 2000)
                        .setWhen(System.currentTimeMillis())
                        .setPriority(Notification.DEFAULT_SOUND)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .build();
                mNotific.notify(ncode, n);


            } else {
                PendingIntent contentIntent = PendingIntent.getActivity(MyFirebaseMessagingService.this, id, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
                NotificationManager nm = (NotificationManager) MyFirebaseMessagingService.this.getSystemService(Context.NOTIFICATION_SERVICE);
                Resources res = MyFirebaseMessagingService.this.getResources();
                NotificationCompat.Builder builder = new NotificationCompat.Builder(MyFirebaseMessagingService.this);
                builder.setContentIntent(contentIntent)
                        .setSmallIcon(R.drawable.app_logo)
                        .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_logo))
                        .setTicker(msg)
                        .setColor(getResources().getColor(R.color.app_color))
                        .setWhen(System.currentTimeMillis())
                        .setAutoCancel(true)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setLights(0xffff0000, 100, 2000)
                        .setPriority(Notification.DEFAULT_SOUND)
                        .setContentText(msg);

                Notification n = builder.getNotification();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    int smallIconViewId = getResources().getIdentifier("right_icon", "id", android.R.class.getPackage().getName());

                    if (smallIconViewId != 0) {
                        if (n.contentView != null)
                            n.contentView.setViewVisibility(smallIconViewId, View.INVISIBLE);

                        if (n.headsUpContentView != null)
                            n.headsUpContentView.setViewVisibility(smallIconViewId, View.INVISIBLE);

                        if (n.bigContentView != null)
                            n.bigContentView.setViewVisibility(smallIconViewId, View.INVISIBLE);
                    }
                }

                n.defaults |= Notification.DEFAULT_ALL;
                nm.notify(id, n);
            }

        } else if (action.equalsIgnoreCase(Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key) || action.equalsIgnoreCase(Iconstant.PushNotification_RideCancelled_Key)) {

            if (Build.VERSION.SDK_INT >= 26) {
                final NotificationManager mNotific =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                final int ncode = 101;
                final String ChannelID = "my_channel_01";
                CharSequence name = "pandiyan";
                String desc = "this is notific";
                int imp = NotificationManager.IMPORTANCE_HIGH;

                @SuppressLint("WrongConstant") NotificationChannel mChannel = new NotificationChannel(ChannelID, name,
                        imp);
                mChannel.setDescription(desc);
                mChannel.setLightColor(Color.CYAN);
                mChannel.canShowBadge();
                mChannel.setShowBadge(true);
                mNotific.createNotificationChannel(mChannel);

                Resources res = getApplicationContext().getResources();

                @SuppressLint("WrongConstant") Notification n = new Notification.Builder(this, ChannelID)
                        .setContentTitle(msg)
                        .setSmallIcon(R.drawable.newapplogo)
                        .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_logo))
                        .setContentText(Job_status_message)
                        .setBadgeIconType(R.drawable.app_logo)
                        .setNumber(5)
                        .setColor(getResources().getColor(R.color.app_color))
                        .setLights(0xffff0000, 100, 2000)
                        .setWhen(System.currentTimeMillis())
                        .setPriority(Notification.DEFAULT_SOUND)
                        .setAutoCancel(true)
                        .build();
                mNotific.notify(ncode, n);


            } else {

                NotificationManager nm = (NotificationManager) MyFirebaseMessagingService.this.getSystemService(Context.NOTIFICATION_SERVICE);
                Resources res = MyFirebaseMessagingService.this.getResources();
                NotificationCompat.Builder builder = new NotificationCompat.Builder(MyFirebaseMessagingService.this);
                builder.setSmallIcon(R.drawable.app_logo)
                        .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_logo))
                        .setTicker(msg)
                        .setColor(getResources().getColor(R.color.app_color))
                        .setWhen(System.currentTimeMillis())
                        .setAutoCancel(true)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setLights(0xffff0000, 100, 2000)
                        .setPriority(Notification.DEFAULT_SOUND)
                        .setContentText(msg);

                Notification n = builder.getNotification();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    int smallIconViewId = getResources().getIdentifier("right_icon", "id", android.R.class.getPackage().getName());

                    if (smallIconViewId != 0) {
                        if (n.contentView != null)
                            n.contentView.setViewVisibility(smallIconViewId, View.INVISIBLE);

                        if (n.headsUpContentView != null)
                            n.headsUpContentView.setViewVisibility(smallIconViewId, View.INVISIBLE);

                        if (n.bigContentView != null)
                            n.bigContentView.setViewVisibility(smallIconViewId, View.INVISIBLE);
                    }
                }

                n.defaults |= Notification.DEFAULT_ALL;
                nm.notify(id, n);
            }
        } else if (action.equalsIgnoreCase(Iconstant.free_wait_finished)) {

            if (Build.VERSION.SDK_INT >= 26) {
                final NotificationManager mNotific =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                final int ncode = 101;
                final String ChannelID = "my_channel_01";
                CharSequence name = "pandiyan";
                String desc = "this is notific";
                int imp = NotificationManager.IMPORTANCE_HIGH;

                @SuppressLint("WrongConstant") NotificationChannel mChannel = new NotificationChannel(ChannelID, name,
                        imp);
                mChannel.setDescription(desc);
                mChannel.setLightColor(Color.CYAN);
                mChannel.canShowBadge();
                mChannel.setShowBadge(true);
                mNotific.createNotificationChannel(mChannel);

                Resources res = getApplicationContext().getResources();

                @SuppressLint("WrongConstant") Notification n = new Notification.Builder(this, ChannelID)
                        .setContentTitle(msg)
                        .setSmallIcon(R.drawable.newapplogo)
                        .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_logo))
                        .setContentText(Job_status_message)
                        .setBadgeIconType(R.drawable.app_logo)
                        .setNumber(5)
                        .setColor(getResources().getColor(R.color.app_color))
                        .setLights(0xffff0000, 100, 2000)
                        .setWhen(System.currentTimeMillis())
                        .setPriority(Notification.DEFAULT_SOUND)
                        .setAutoCancel(true)
                        .build();
                mNotific.notify(ncode, n);


            } else {

                NotificationManager nm = (NotificationManager) MyFirebaseMessagingService.this.getSystemService(Context.NOTIFICATION_SERVICE);
                Resources res = MyFirebaseMessagingService.this.getResources();
                NotificationCompat.Builder builder = new NotificationCompat.Builder(MyFirebaseMessagingService.this);
                builder.setSmallIcon(R.drawable.app_logo)
                        .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_logo))
                        .setTicker(msg)
                        .setColor(getResources().getColor(R.color.app_color))
                        .setWhen(System.currentTimeMillis())
                        .setAutoCancel(true)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setLights(0xffff0000, 100, 2000)
                        .setPriority(Notification.DEFAULT_SOUND)
                        .setContentText(msg);

                Notification n = builder.getNotification();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    int smallIconViewId = getResources().getIdentifier("right_icon", "id", android.R.class.getPackage().getName());

                    if (smallIconViewId != 0) {
                        if (n.contentView != null)
                            n.contentView.setViewVisibility(smallIconViewId, View.INVISIBLE);

                        if (n.headsUpContentView != null)
                            n.headsUpContentView.setViewVisibility(smallIconViewId, View.INVISIBLE);

                        if (n.bigContentView != null)
                            n.bigContentView.setViewVisibility(smallIconViewId, View.INVISIBLE);
                    }
                }

                n.defaults |= Notification.DEFAULT_ALL;
                nm.notify(id, n);
            }
        }


    }


    private void removeNotification(final int id, final NotificationManager notificationManager) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                notificationManager.cancel(id);
            }
        }, 10000);
    }


    public int createID() {
        Date now = new Date();
        int id = Integer.parseInt(new SimpleDateFormat("ddHHmmss", Locale.US).format(now));
        return id;
    }
}











