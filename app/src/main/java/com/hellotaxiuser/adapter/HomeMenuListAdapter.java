package com.hellotaxiuser.adapter;

/**
 * Created by Prem Kumar on 10/1/2015.
 */

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hellotaxiuser.utils.SessionManager;
import com.hellotaxiuser.app.R;
import com.mylibrary.widgets.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.HashMap;


/**
 * Created by Prem Kumar on 9/21/2015.
 */
public class HomeMenuListAdapter extends BaseAdapter {
    Context context;
    String[] mTitle;
    int[] mIcon;
    private String UserprofileImage, User_phone, User_phoneCode, User_fullname, wallet_money;

    public HomeMenuListAdapter(Context context, String[] title, int[] icon) {
        this.context = context;
        this.mTitle = title;
        this.mIcon = icon;

    }

    @Override
    public int getCount() {
        return mTitle.length;
    }

    @Override
    public Object getItem(int position) {
        return mTitle[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Declare Variables


        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder = new ViewHolder();
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.drawer_list_item, parent, false);

            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
            holder.walletMoney = (TextView) convertView.findViewById(R.id.drawer_item_list_wallet_money);
            holder.profile_mobile = (TextView) convertView.findViewById(R.id.profile_mobile_number);
            holder.imgIcon = (ImageView) convertView.findViewById(R.id.icon);
            holder.profile_name = (TextView) convertView.findViewById(R.id.profile_name);
            holder.profile_icon = (RoundedImageView) convertView.findViewById(R.id.profile_icon);
            holder.general_layout = (RelativeLayout) convertView.findViewById(R.id.drawer_list_item_normal_layout);
            holder.profile_layout = (RelativeLayout) convertView.findViewById(R.id.drawer_list_item_profile_layout);
            holder.drawer_view = (View) convertView.findViewById(R.id.drawer_list_view);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position == 0) {


            holder.profile_layout.setVisibility(View.VISIBLE);
            holder.general_layout.setVisibility(View.GONE);
            holder.drawer_view.setVisibility(View.GONE);


            SessionManager session = new SessionManager(context);
            HashMap<String, String> user = session.getUserDetails();
            UserprofileImage = user.get(SessionManager.KEY_USERIMAGE);
            User_phone = user.get(SessionManager.KEY_PHONENO);
            User_phoneCode = user.get(SessionManager.KEY_COUNTRYCODE);
            User_fullname = user.get(SessionManager.KEY_USERNAME);


            Picasso.with(context).load(String.valueOf(UserprofileImage)).placeholder(R.drawable.no_profile_image_avatar_icon).into(holder.profile_icon);
            holder.profile_name.setText(User_fullname);
            holder.profile_mobile.setText(User_phoneCode + " " + User_phone);
        } else {

            if (position == 3) {
                holder.drawer_view.setVisibility(View.VISIBLE);
                holder.walletMoney.setVisibility(View.VISIBLE);
            } else {
                holder.drawer_view.setVisibility(View.GONE);
                holder.walletMoney.setVisibility(View.GONE);
            }

            holder.profile_layout.setVisibility(View.GONE);
            holder.general_layout.setVisibility(View.VISIBLE);


            holder.imgIcon.setImageResource(mIcon[position]);
            int color = Color.parseColor("#000000"); //The color u want
            holder.imgIcon.setColorFilter(color);
            holder.txtTitle.setText(mTitle[position]);

            SessionManager session = new SessionManager(context);
            HashMap<String, String> amount = session.getWalletAmount();
            wallet_money = amount.get(SessionManager.KEY_WALLET_AMOUNT);
            holder.walletMoney.setText(wallet_money);
        }

        return convertView;
    }

    static class ViewHolder {
        TextView txtTitle, profile_name, profile_mobile, walletMoney;
        RoundedImageView profile_icon;
        ImageView imgIcon;
        RelativeLayout general_layout, profile_layout;
        View drawer_view;
    }
}

