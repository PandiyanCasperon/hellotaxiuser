package com.hellotaxiuser.app;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hellotaxiuser.iconstant.Iconstant;
import com.hellotaxiuser.utils.AppInfoSessionManager;
import com.hellotaxiuser.utils.SessionManager;
import com.hellotaxiuser.app.R;

/**
 */
public class AboutUs extends Activity {
    private RelativeLayout back;
    private TextView Tv_more_info, version_code, Tv_About_us_content, tv_customer_phone, tv_customer_address, tv_terms_condition;
    AppInfoSessionManager appInfo_Session;
    private String currentVersion;
    SessionManager session;
    private String customerPhoneNo = "";
    final int PERMISSION_REQUEST_CODE = 111;

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aboutus);
//        ChatService.startUserAction(AboutUs.this);
        back = (RelativeLayout) findViewById(R.id.aboutus_header_back_layout);
        Tv_more_info = (TextView) findViewById(R.id.more_info_baseurl);
        Tv_About_us_content = (TextView) findViewById(R.id.textView);
        version_code = (TextView) findViewById(R.id.aboutus_versioncode);

        tv_customer_phone = (TextView) findViewById(R.id.more_phone_baseurl);
        tv_customer_address = (TextView) findViewById(R.id.more_address_baseurl);
        tv_terms_condition = (TextView) findViewById(R.id.aboutus_terms_condition);

        appInfo_Session = new AppInfoSessionManager(AboutUs.this);
        session = new SessionManager(AboutUs.this);
        try {
            currentVersion = AboutUs.this.getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        version_code.setText(getResources().getString(R.string.aboutus_lable_version_textview) + " " + currentVersion);


        String second_text = "<font color='#012D8E'>" + getResources().getString(R.string.login_label_Terms_of_service) + " " + "</font>";
        String tired_text = "<font color='#5b5b5b'>" + getResources().getString(R.string.login_label_and) + " " + "</font>";
        String fourth_text = "<font color='#012D8E'>" + getResources().getString(R.string.login_label_Privacy_Policy) + "</font>";
        tv_terms_condition.setText(Html.fromHtml( second_text + tired_text + fourth_text));

        ClickableSpan termsOfServicesClick = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Iconstant.terms_of_servie_URL));
                startActivity(browserIntent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        };

        ClickableSpan privacyPolicyClick = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Iconstant.privacy_policy_url));
                startActivity(browserIntent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        };


        makeLinks(tv_terms_condition, new String[]{getResources().getString(R.string.login_label_Terms_of_service), getResources().getString(R.string.login_label_Privacy_Policy)}, new ClickableSpan[]{
                termsOfServicesClick, privacyPolicyClick
        });



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
    }

    public void makeLinks(TextView textView, String[] links, ClickableSpan[] clickableSpans) {
        SpannableString spannableString = new SpannableString(textView.getText());
        for (int i = 0; i < links.length; i++) {
            ClickableSpan clickableSpan = clickableSpans[i];
            String link = links[i];

            int startIndexOfLink = textView.getText().toString().indexOf(link);

            spannableString.setSpan(clickableSpan, startIndexOfLink, startIndexOfLink + link.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.app_color)), startIndexOfLink, startIndexOfLink + link.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spannableString, TextView.BufferType.SPANNABLE);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            onBackPressed();
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }
}


