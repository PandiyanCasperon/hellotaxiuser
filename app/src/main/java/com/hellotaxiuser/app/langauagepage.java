package com.hellotaxiuser.app;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hellotaxiuser.utils.ConnectionDetector;
import com.hellotaxiuser.utils.SessionManager;
import com.hellotaxiuser.app.R;
import com.countrycodepicker.CountryPicker;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.mylibrary.gps.GPSTracker;
import com.mylibrary.volley.ServiceRequest;


/**
 * Created by Prem Kumar and Anitha on 10/12/2015.
 */
public class langauagepage extends FragmentActivity{
    private ImageView ham_home;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private EditText Et_name, Et_phoneNo, Et_emailId;
    private RelativeLayout Rl_save_edit;
    private RelativeLayout change_layout;
    private ServiceRequest mRequest;
    Dialog dialog;
    private String UserID = "";
    private TextView tv_code;

    private double MyCurrent_lat = 0.0, MyCurrent_long = 0.0;

    GPSTracker gps;

    //-----Declaration For Enabling Gps-------
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;

    private ImageView Img_send_notification;


    CountryPicker picker;
    private TextView Tv_save_edit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changelanguagef);
        initialize();

        //Start XMPP Chat Service
//        ChatService.startUserAction(EmergencyContact.this);


        change_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(ham_home.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                onBackPressed();
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        ham_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(ham_home.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                onBackPressed();
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


    }

    private void initialize() {

        ham_home = (ImageView) findViewById(R.id.ham_home);
        change_layout = (RelativeLayout) findViewById(R.id.change_layout);

    }




}
