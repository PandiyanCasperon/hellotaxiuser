package com.hellotaxiuser.app;

/**
 * Created by Prem Kumar on 10/1/2015.
 */

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.hellotaxiuser.utils.SessionManager;

public class SignupAndSignin extends Activity {
    public static SignupAndSignin activty;
    SessionManager session;
    //private SimpleGestureFilter detector;
    private TextView signin, register;
    private String Str_Hash = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_signin);
        activty = this;
        initialize();
        signin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SignupAndSignin.this, LoginPage.class);
                i.putExtra("HashKey", Str_Hash);
                startActivity(i);
            }
        });

        register.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SignupAndSignin.this, RegisterPage.class);
                startActivity(i);
            }
        });

    }

    private void initialize() {
        signin = (TextView) findViewById(R.id.signin_main_button);
        register = (TextView) findViewById(R.id.register_main_button);
        session = new SessionManager(getApplicationContext());
        Intent i = getIntent();
        Str_Hash = i.getStringExtra("HashKey");
        System.out.println("Str_Hash--------------------" + Str_Hash);
        signin.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf"));
        register.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf"));
    }

}
