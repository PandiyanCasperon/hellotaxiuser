package com.hellotaxiuser.app;


import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.hellotaxiuser.DataBase.DataBaseHelper;
import com.hellotaxiuser.iconstant.Iconstant;
import com.hellotaxiuser.pojo.EmergencyPojo;
import com.hellotaxiuser.utils.AppInfoSessionManager;
import com.hellotaxiuser.utils.ConnectionDetector;
import com.hellotaxiuser.utils.Getlocation;
import com.hellotaxiuser.utils.IdentifyAppKilled;
import com.hellotaxiuser.utils.Locationextends_activity;
import com.hellotaxiuser.utils.SessionManager;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationSettingsResult;
import com.mylibrary.dialog.PkDialog;
import com.mylibrary.dialog.PkDialogWithoutButton;
import com.mylibrary.gps.GPSTracker;
import com.mylibrary.volley.ServiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;


public class SplashPage extends Locationextends_activity implements Getlocation.Callbacks {
    public static String PHONEMASKINGSTATUS = "";
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 500;
    final int PERMISSION_REQUEST_CODE = 111;
    SessionManager session;
    Context context;
    GPSTracker gps;
    PendingResult<LocationSettingsResult> result;
    AppInfoSessionManager appInfo_Session;
    PkDialogWithoutButton mInfoDialog;
    String sPendingRideId = "", sRatingStatus = "", sCategoryImage = "", sOngoingRide = "", sOngoingRideId = "", app_identity_name = "";
    ImageView ivSplashImage;
    private String userID = "", sLatitude = "", sLongitude = "";
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    private String Str_HashKey = "", currentVersion = "";
    private boolean isAppInfoAvailable = false, locationUpdated = false;
    private String server_mode, site_mode, site_string, site_url, About_Content = "", user_image = "", userName = "", Language_code = "";
    private boolean isNeedBanner = true;
    private ArrayList<EmergencyPojo> emergencyAraryList;
    private boolean isEmergencyAvailabe = false;
    private String sPage = "", sType = "", sRideId = "";
    private String title = "", msg = "", banner = "";
    private String onlineVersion = "";
    public static SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.splash);

        sessionManager = new SessionManager(getApplicationContext());
        sessionManager.setfirsttimeloader("1");

        initialization();

        Intent i = getIntent();
        if (i != null) {
            if (i.hasExtra("page")) {
                sPage = i.getStringExtra("page");
                sType = i.getStringExtra("type");
                if (sPage.equalsIgnoreCase("Ads")) {
                    title = i.getStringExtra("title");
                    msg = i.getStringExtra("msg");
                    banner = i.getStringExtra("banner");
                    session.setADS(true);
                    session.setAds(title, msg, banner);
                } else {
                    sRideId = i.getStringExtra("rideId");
                }
            }

        }

    }

    private void initialization() {
        try {
            ivSplashImage = findViewById(R.id.ivSplashImg);
            context = getApplicationContext();
            cd = new ConnectionDetector(SplashPage.this);
            isInternetPresent = cd.isConnectingToInternet();
            // Session class instance
            session = new SessionManager(getApplicationContext());
            appInfo_Session = new AppInfoSessionManager(SplashPage.this);
            emergencyAraryList = new ArrayList<EmergencyPojo>();
            currentVersion = SplashPage.this.getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }




//        Glide.with(SplashPage.this).load(R.mipmap.splash).listener(new RequestListener<Drawable>() {
//
//            @Override
//            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                return false;
//            }
//
//            @Override
//            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, com.bumptech.glide.load.DataSource dataSource, boolean isFirstResource) {
//                ((GifDrawable) resource).setLoopCount(1);
//                return false;
//            }
//
//        }).into(ivSplashImage);


        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                Str_HashKey = (Base64.encodeToString(md.digest(), Base64.DEFAULT));
                System.out.println("Str_HashKey--------------" + Str_HashKey);


            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateClient(Location location) {
        if (location != null) {
            if (session == null) {
                session = new SessionManager(getApplicationContext());
            }
            if (!locationUpdated && !String.valueOf(session.getLatitude()).equalsIgnoreCase("")) {
                locationUpdated = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setLocation();
                    }
                }, SPLASH_TIME_OUT);
            }
        }
    }


    private void Alert(String title, String alert) {
        try {
            final PkDialog mDialog = new PkDialog(SplashPage.this);
            mDialog.setDialogTitle(title);
            mDialog.setDialogMessage(alert);
            mDialog.setPositiveButton(getResources().getString(R.string.alert_label_ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    finish();
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }
            });
            mDialog.setNegativeButton(getResources().getString(R.string.couponcode_label_cancel), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    finish();
                }
            });
            mDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setLocation() {
        cd = new ConnectionDetector(SplashPage.this);
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {

            gps = new GPSTracker(SplashPage.this);
            if (gps.isgpsenabled() && gps.canGetLocation()) {
                if (session.isLoggedIn()) {
                    HashMap<String, String> user = session.getUserDetails();
                    userID = user.get(SessionManager.KEY_USERID);
                    sLatitude = String.valueOf(gps.getLatitude());
                    sLongitude = String.valueOf(gps.getLongitude());
                    postRequest_AppInformation(Iconstant.app_info_url);
                } else {
                    HashMap<String, String> user = session.getUserDetails();
                    userID = user.get(SessionManager.KEY_USERID);
                    sLatitude = String.valueOf(gps.getLatitude());
                    sLongitude = String.valueOf(gps.getLongitude());
                    postRequest_AppInformation(Iconstant.app_info_url);
                }
            }
        } else {
            final PkDialog mDialog = new PkDialog(SplashPage.this);
            mDialog.setDialogTitle(getResources().getString(R.string.alert_nointernet));
            mDialog.setDialogMessage(getResources().getString(R.string.alert_nointernet_message));
            mDialog.setPositiveButton(getResources().getString(R.string.timer_label_alert_retry), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    setLocation();
                }
            });
            mDialog.setNegativeButton(getResources().getString(R.string.timer_label_alert_cancel), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    finish();
                }
            });
            mDialog.show();

        }
    }


    private void intentMethods() {

        if ("track".equalsIgnoreCase(sPage)) {
            Intent i = new Intent(SplashPage.this, MyRideDetailTrackRide.class);
            i.putExtra("rideID", sRideId);
            i.putExtra("type", sType);
            startActivity(i);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if ("farebreakup".equalsIgnoreCase(sPage)) {
            Intent i = new Intent(SplashPage.this, FareBreakUp.class);
            i.putExtra("RideID", sRideId);
            i.putExtra("type", sType);
            startActivity(i);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if ("rating".equalsIgnoreCase(sPage)) {
            Intent i = new Intent(SplashPage.this, MyRideRating.class);
            i.putExtra("RideID", sRideId);
            i.putExtra("type", sType);
            startActivity(i);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if ("Ads".equalsIgnoreCase(sPage)) {
            Intent i1 = new Intent(SplashPage.this, AdsPage.class);
            i1.putExtra("AdsTitle", title);
            i1.putExtra("AdsMessage", msg);
            i1.putExtra("type", sType);
            if (!"".equalsIgnoreCase(banner)) {
                i1.putExtra("AdsBanner", banner);
            }
            i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i1);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if ("details".equalsIgnoreCase(sPage)) {
            Intent i = new Intent(SplashPage.this, MyRidesDetail.class);
            i.putExtra("RideID", sRideId);
            i.putExtra("type", sType);
            startActivity(i);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else {
            Intent i = new Intent(SplashPage.this, UpdateUserLocation.class);
            startActivity(i);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }


    //-----------------------App Information Post Request-----------------
    private void postRequest_AppInformation(String Url) {

        System.out.println("-------------Splash App Information Url----------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_type", "user");
        jsonParams.put("id", userID);
        jsonParams.put("lat", sLatitude);
        jsonParams.put("lon", sLongitude);
        System.out.println("-------------appinfo---------jsonParams-------" + jsonParams);

        mRequest = new ServiceRequest(SplashPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------SplashPage appinfo----------------" + response);
                String Str_status = "", sContact_mail = "", customer_service_number = "", customer_service_address = "", sShare_status = "", sCustomerServiceNumber = "", sSiteUrl = "", sXmppHostUrl = "", sHostName = "", sFacebookId = "", sGooglePlusId = "", sPhoneMasking = "";
                DataBaseHelper dataBaseHelper = new DataBaseHelper(getApplicationContext());
                long id = dataBaseHelper.InsertJson("AppInfo", response);
                Log.d("DataBase", String.valueOf(id));
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            JSONObject info_object = response_object.getJSONObject("info");
                            if (info_object.length() > 0) {
                                sContact_mail = info_object.getString("site_contact_mail");
                                sCustomerServiceNumber = info_object.getString("customer_service_number");
                                sSiteUrl = info_object.getString("site_url");
                                sXmppHostUrl = info_object.getString("xmpp_host_url");
                                sHostName = info_object.getString("xmpp_host_name");
                                sFacebookId = info_object.getString("facebook_id");
                                sGooglePlusId = info_object.getString("google_plus_app_id");
                                app_identity_name = info_object.getString("app_identity_name");
                                server_mode = info_object.getString("server_mode");
                                site_mode = info_object.getString("site_mode");
                                site_string = info_object.getString("site_mode_string");
                                site_url = info_object.getString("site_url");
                                About_Content = info_object.getString("about_content");
                                user_image = info_object.getString("user_image");
                                userName = info_object.getString("user_name");
                                Language_code = info_object.getString("lang_code");
                                onlineVersion = info_object.getString("user_version_control");
                                if (info_object.has("customer_service_number")) {
                                    customer_service_number = info_object.getString("customer_service_number");
                                }
                                if (info_object.has("site_contact_address")) {
                                    customer_service_address = info_object.getString("site_contact_address");
                                }
                                if (info_object.has("pooling")) {
                                    sShare_status = info_object.getString("pooling");
                                } else {
                                    sShare_status = "0";
                                }
                                if (info_object.has("phone_masking_status")) {
                                    String phoneMaskingStatus = info_object.getString("phone_masking_status");
                                    PHONEMASKINGSTATUS = phoneMaskingStatus;
                                    // set phone masking
                                    session.setKeyPhoneMaskingStatus(PHONEMASKINGSTATUS);
                                    System.out.println("=====>>>===PHONEMASKINGSTATUS ==========>>>>> " + PHONEMASKINGSTATUS);
                                }
                                Object emercencyObject = info_object.get("emergency_numbers");
                                if (emercencyObject instanceof JSONArray) {
                                    JSONArray emercency_array = info_object.getJSONArray("emergency_numbers");
                                    if (emercency_array.length() > 0) {
                                        emergencyAraryList.clear();
                                        for (int j = 0; j < emercency_array.length(); j++) {
                                            JSONObject job = emercency_array.getJSONObject(j);
                                            EmergencyPojo emergencyPojo = new EmergencyPojo();
                                            emergencyPojo.setTitle(job.getString("title"));
                                            emergencyPojo.setNumber(job.getString("number"));
                                            emergencyAraryList.add(emergencyPojo);
                                        }
                                        isEmergencyAvailabe = true;
                                    } else {
                                        isEmergencyAvailabe = false;
                                    }
                                }
                                isAppInfoAvailable = true;
                            } else {
                                isAppInfoAvailable = false;
                            }
                        } else {
                            isAppInfoAvailable = false;
                        }
                    } else {
                        isAppInfoAvailable = false;
                    }

                    if (!onlineVersion.equals("")) {
                        if (onlineVersion != null && !onlineVersion.isEmpty()) {
                            if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {
                                if (SplashPage.this != null && !SplashPage.this.isFinishing()) {

                                    Alert(getResources().getString(R.string.app_name), "There is newer version of this application available, click OK to upgrade now?");
                                }
                                //                Alert(getResources().getString(R.string.app_name), "There is newer version of this application available, click OK to upgrade now?");
                            } else {
                                if (Str_status.equalsIgnoreCase("1") && isAppInfoAvailable) {


                                    HashMap<String, String> language = session.getLanaguage();
                                    Locale locale = null;

                                    switch (Language_code) {

                                        case "en":
                                            locale = new Locale("en");
                                            session.setlamguage("en", "en");
                                            break;
                                        case "es":
                                            locale = new Locale("es");
                                            session.setlamguage("es", "es");
                                            break;
                                        case "ta":
                                            locale = new Locale("ta");
                                            session.setlamguage("ta", "ta");
                                            break;
                                        default:
                                            locale = new Locale("en");
                                            session.setlamguage("en", "en");
                                            break;
                                    }

                                    Locale.setDefault(locale);
                                    Configuration config = new Configuration();
                                    config.locale = locale;
                                    getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());

                                    appInfo_Session.setAppInfo(sContact_mail, sCustomerServiceNumber, sSiteUrl, sXmppHostUrl, sHostName, sFacebookId, sGooglePlusId, sCategoryImage, sOngoingRide, sOngoingRideId, sPendingRideId, sRatingStatus);
                                    session.setXmpp(sXmppHostUrl, sHostName);
                                    session.setAgent(app_identity_name);
                                    session.setAbout(About_Content);
                                    session.setcustomerdetail(customer_service_number, customer_service_address);
                                    session.setuser_image(user_image);
                                    session.setUserNameUpdate(userName);
                                    session.setShareStatus(sShare_status);
                                    if (isEmergencyAvailabe) {
                                        session.putEmergencyContactDetails(emergencyAraryList);
                                    }
                                    if (site_mode.equalsIgnoreCase("development")) {
                                        mInfoDialog = new PkDialogWithoutButton(SplashPage.this);
                                        mInfoDialog.setDialogTitle("ALERT");
                                        mInfoDialog.setDialogMessage(site_string);
                                        mInfoDialog.show();
                                    } else {
                                        if (session.isLoggedIn()) {
                                            intentMethods();
                                        } else {


                                            if (isNeedBanner) {
                                                Intent i = new Intent(SplashPage.this, SignUpBannerPage.class);
                                                startActivity(i);
                                                finish();
                                                overridePendingTransition(R.anim.enter, R.anim.exit);

                                            } else {
                                                Intent i = new Intent(SplashPage.this, SignupAndSignin.class);
                                                startActivity(i);
                                                finish();
                                                overridePendingTransition(R.anim.enter, R.anim.exit);
                                            }
                                        }
                                    }
                                    if (server_mode.equalsIgnoreCase("0")) {
                                        Toast.makeText(context, site_url, Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    if (!((Activity) context).isFinishing()) {
                                        swtDialogSucces();
                                    }
                                }
                            }
                        } else {
                            Toast.makeText(context, "Not able to get current version", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (Str_status.equalsIgnoreCase("1") && isAppInfoAvailable) {


                            HashMap<String, String> language = session.getLanaguage();
                            Locale locale = null;

                            switch (Language_code) {

                                case "en":
                                    locale = new Locale("en");
                                    session.setlamguage("en", "en");
                                    break;
                                case "es":
                                    locale = new Locale("es");
                                    session.setlamguage("es", "es");
                                    break;
                                case "ta":
                                    locale = new Locale("ta");
                                    session.setlamguage("ta", "ta");
                                    break;
                                default:
                                    locale = new Locale("en");
                                    session.setlamguage("en", "en");
                                    break;
                            }

                            Locale.setDefault(locale);
                            Configuration config = new Configuration();
                            config.locale = locale;
                            getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());

                            appInfo_Session.setAppInfo(sContact_mail, sCustomerServiceNumber, sSiteUrl, sXmppHostUrl, sHostName, sFacebookId, sGooglePlusId, sCategoryImage, sOngoingRide, sOngoingRideId, sPendingRideId, sRatingStatus);
                            session.setXmpp(sXmppHostUrl, sHostName);
                            session.setAgent(app_identity_name);
                            session.setAbout(About_Content);
                            session.setcustomerdetail(customer_service_number, customer_service_address);
                            session.setuser_image(user_image);
                            session.setUserNameUpdate(userName);


                            session.setShareStatus(sShare_status);

                            if (isEmergencyAvailabe) {
                                session.putEmergencyContactDetails(emergencyAraryList);
                            }

                            if (site_mode.equalsIgnoreCase("development")) {
                                mInfoDialog = new PkDialogWithoutButton(SplashPage.this);
                                mInfoDialog.setDialogTitle("ALERT");
                                mInfoDialog.setDialogMessage(site_string);
                                mInfoDialog.show();
                            } else {
                                if (session.isLoggedIn()) {
                                    intentMethods();
                                } else {


                                    if (isNeedBanner) {
                                        Intent i = new Intent(SplashPage.this, SignUpBannerPage.class);
                                        startActivity(i);
                                        finish();
                                        overridePendingTransition(R.anim.enter, R.anim.exit);

                                    } else {
                                        Intent i = new Intent(SplashPage.this, SignupAndSignin.class);
                                        startActivity(i);
                                        finish();
                                        overridePendingTransition(R.anim.enter, R.anim.exit);
                                    }

                                }

                            }
                            if (server_mode.equalsIgnoreCase("0")) {
                                Toast.makeText(context, site_url, Toast.LENGTH_SHORT).show();
                            }
                        } else {

                            if (!((Activity) context).isFinishing()) {
                                swtDialogSucces();
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                try {
                    swtDialogSucces();
                } catch (ClassCastException e1) {
                    e1.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }


    private void swtDialogSucces() {
        mInfoDialog = new PkDialogWithoutButton(SplashPage.this);
        mInfoDialog.setDialogTitle(getResources().getString(R.string.app_info_header_textView));
        mInfoDialog.setDialogMessage(getResources().getString(R.string.app_info_content));
        mInfoDialog.show();
    }

//----------------------------------------------

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                System.out.println("1 already running");
                b = true;
                break;
            } else {
                System.out.println("2 not running");
                b = false;
            }
        }
        System.out.println("3 not running");
        return b;
    }

    @Override
    protected void onResume() {
        super.onResume();
        //--------Start Service to identify app killed or not---------
        if (!isMyServiceRunning(IdentifyAppKilled.class)) {
            startService(new Intent(SplashPage.this, IdentifyAppKilled.class));
        }
    }

    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            return true;
        }
        return false;
    }


}