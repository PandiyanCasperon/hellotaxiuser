package com.hellotaxiuser.utils;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.yayandroid.locationmanager.base.LocationBaseService;
import com.yayandroid.locationmanager.configuration.Configurations;
import com.yayandroid.locationmanager.configuration.LocationConfiguration;
import com.yayandroid.locationmanager.constants.FailType;
import com.yayandroid.locationmanager.constants.ProcessType;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class Locationservices extends LocationBaseService {

    public static final String ACTION_LOCATION_CHANGED = "com.yayandroid.locationmanager.sample.service.LOCATION_CHANGED";
    public static final String ACTION_LOCATION_FAILED = "com.yayandroid.locationmanager.sample.service.LOCATION_FAILED";
    public static final String ACTION_PROCESS_CHANGED = "com.yayandroid.locationmanager.sample.service.PROCESS_CHANGED";

    public static final String EXTRA_LOCATION = "ExtraLocationField";
    public static final String EXTRA_FAIL_TYPE = "ExtraFailTypeField";
    public static final String EXTRA_PROCESS_TYPE = "ExtraProcessTypeField";

    private boolean isLocationRequested = false;
    private SessionManager sessionManager;
    public static Context mcontext;
    public Locationservices() {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public LocationConfiguration getLocationConfiguration() {
        return Configurations.silentConfiguration(false);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // calling super is required when extending from LocationBaseService
        super.onStartCommand(intent, flags, startId);

        sessionManager = new SessionManager(Locationservices.this);
        getLocation();
     /*   if(!isLocationRequested) {
            isLocationRequested = true;
            getLocation();
        }
*/
        // Return type is depends on your requirements
        return START_NOT_STICKY;
    }

    @Override
    public void onLocationChanged(Location location) {
//        Intent intent = new Intent(ACTION_LOCATION_CHANGED);
//        intent.putExtra(EXTRA_LOCATION, location);
//        sendBroadcast(intent);
        sessionManager.setLatitude(String.valueOf(location.getLatitude()));
        sessionManager.setLongitude(String.valueOf(location.getLongitude()));
        sessionManager.setclearAddress();
        getAddress(location.getLatitude(),location.getLongitude());


      /*  Toast.makeText(getApplicationContext(),"latitude:"+location.getLatitude(),Toast.LENGTH_SHORT).show();
        Toast.makeText(getApplicationContext(),"Longitude:"+location.getLongitude(),Toast.LENGTH_SHORT).show();*/
//        stopSelf();
    }

    @Override
    public void onLocationFailed(@FailType int type) {
//        Intent intent = new Intent(ACTION_LOCATION_FAILED);
//        intent.putExtra(EXTRA_FAIL_TYPE, type);
//        sendBroadcast(intent);

//        stopSelf();
    }

    @Override
    public void onProcessTypeChanged(@ProcessType int processType) {
//        Intent intent = new Intent(ACTION_PROCESS_CHANGED);
//        intent.putExtra(EXTRA_PROCESS_TYPE, processType);
//        sendBroadcast(intent);
    }

    /**
     * Function to get latitude
     */

    public void getAddress(double lat, double lng) {
        try {
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
                Address obj = addresses.get(0);
                String address = obj.getAddressLine(0);
                sessionManager.setAddress(address);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
