package com.hellotaxiuser.newoptimization.homepage.listeners;

public interface HomePageMapMoveInterface {
    public void mapIsTouched(boolean b);
}
