package com.hellotaxiuser.newoptimization.homepage.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.widget.FrameLayout;

import com.hellotaxiuser.newoptimization.homepage.listeners.HomePageMapMoveInterface;

public class TouchableWrapper extends FrameLayout {
    HomePageMapMoveInterface homePageMapMoveInterface;
    public TouchableWrapper(@NonNull Context context, HomePageMapMoveInterface homePageMapMoveInterface) {
        super(context);
        this.homePageMapMoveInterface=homePageMapMoveInterface;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                homePageMapMoveInterface.mapIsTouched(true);
                break;
            case MotionEvent.ACTION_UP:
                homePageMapMoveInterface.mapIsTouched(true);
                break;
        }

        return super.dispatchTouchEvent(ev);

    }

}