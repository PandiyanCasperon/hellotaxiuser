package com.hellotaxiuser.airportmodule.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hellotaxiuser.airportmodule.interfaces.AirportAddressClickInterface;
import com.hellotaxiuser.airportmodule.model.AirportAddressModel;
import com.hellotaxiuser.app.R;


import java.util.ArrayList;

public class AirportPickupSelectRecylerAdapter extends RecyclerView.Adapter<AirportPickupSelectRecylerAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<AirportAddressModel> airportAddressModels;
    private AirportAddressClickInterface airportAddressClickInterface;
    private int selectedPos = 0;

    public AirportPickupSelectRecylerAdapter(Context context, ArrayList<AirportAddressModel> airportAddressModels, AirportAddressClickInterface airportAddressClickInterface) {
        mContext = context;
        this.airportAddressModels = airportAddressModels;
        this.airportAddressClickInterface = airportAddressClickInterface;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.airport_select_pickup_recyler_adaper, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {
        myViewHolder.address_tv.setText(airportAddressModels.get(i).getAddress());
        if (i == getItemCount() - 1) {
            myViewHolder.seperator_view.setVisibility(View.GONE);

        } else {
            myViewHolder.seperator_view.setVisibility(View.VISIBLE);
        }
        if (selectedPos == i) {
            myViewHolder.selected_tick_iv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.airport_ic_address_select_tick));
        } else {
            myViewHolder.selected_tick_iv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.airport_ic_address_unselect));

        }
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                airportAddressClickInterface.clickAddress(i, airportAddressModels.get(i).getAddress(), airportAddressModels.get(i).getLat(), airportAddressModels.get(i).getLon());
                selectedPos = i;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return airportAddressModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView address_tv;
        ImageView selected_tick_iv;
        View seperator_view;

        public MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            address_tv = (TextView) itemView.findViewById(R.id.address_tv);
            selected_tick_iv = (ImageView) itemView.findViewById(R.id.address_select_tick_iv);
            seperator_view = (View) itemView.findViewById(R.id.seperator_view);
        }
    }
}
