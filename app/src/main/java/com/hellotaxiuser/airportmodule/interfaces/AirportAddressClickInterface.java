package com.hellotaxiuser.airportmodule.interfaces;

public interface AirportAddressClickInterface {

    public void clickAddress(int position,String address, String lat,String lon);
}
