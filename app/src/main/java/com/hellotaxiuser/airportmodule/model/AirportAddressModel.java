package com.hellotaxiuser.airportmodule.model;

public class AirportAddressModel {
    private String address, lat, lon;

    public AirportAddressModel(String address, String lat, String lon) {
        this.address = address;
        this.lat = lat;
        this.lon = lon;
    }

    public String getAddress() {
        return address;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }
}
