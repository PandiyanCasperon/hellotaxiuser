package com.hellotaxiuser.airportmodule.model;

public class LocationBoundarySubPojo {

    private double latitude=0.0;
    private double logintude=0.0;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLogintude() {
        return logintude;
    }

    public void setLogintude(double logintude) {
        this.logintude = logintude;
    }
}
