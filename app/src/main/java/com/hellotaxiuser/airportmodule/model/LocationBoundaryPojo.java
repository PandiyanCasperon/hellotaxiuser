package com.hellotaxiuser.airportmodule.model;

import java.util.ArrayList;

public class LocationBoundaryPojo {

    private ArrayList<LocationBoundarySubPojo> boundary_array_list;

    public ArrayList<LocationBoundarySubPojo> getBoundary_array_list() {
        return boundary_array_list;
    }

    public void setBoundary_array_list(ArrayList<LocationBoundarySubPojo> boundary_array_list) {
        this.boundary_array_list = boundary_array_list;
    }


}
